const model = require("./model");
const http = require("http");
const httpCodes = require("http-status-codes");
const constants = require("./model/constants");
const moment = require("moment");

let handleRequest = (request, response) => {

    let responseType = constants.responseTypes.text;
    let responseContent = "";
    let responseCode = httpCodes.OK;
    
    let managedResponse = {
        responseCode: "",
        responseContent: ""
    };

    let endpoint = "";
    let parameter = "";
    let index = request.url.lastIndexOf("/");

    if(index>0){
        endpoint = request.url.substring(0, index).toLowerCase();
        parameter = request.url.substring(index+1).toLowerCase();
    }else{
        endpoint = request.url.toLowerCase();
    }

    switch(endpoint){
        case constants.endpoints.tasks:
            managedResponse = manageTasks(request.method);
            response.writeHead(managedResponse.responseCode, {"Content-Type": responseType});    
            response.write(JSON.stringify(managedResponse.responseContent));        
            response.end();
            break;
        case constants.endpoints.task:
            let body = [];
            request.on('data', (chunk) => {
                body.push(chunk);
            }).on('end', () => {
                body = Buffer.concat(body).toString();
                if(body != ""){
                    body = JSON.parse(body);
                }
                managedResponse = manageTask(request.method, parameter, body);
                response.writeHead(managedResponse.responseCode, {"Content-Type": responseType});    
                response.write(JSON.stringify(managedResponse.responseContent));        
                response.end();          
            });
            break;
        case constants.endpoints.currents:
            managedResponse = manageCurrents(request.method, parameter);
            response.writeHead(managedResponse.responseCode, {"Content-Type": responseType});    
            response.write(JSON.stringify(managedResponse.responseContent));        
            response.end();
            break;
        default:
            response.writeHead(httpCodes.BAD_REQUEST, {"Content-Type": responseType});    
            response.write(JSON.stringify({ error: true, mensajeError: "Error: el endpoint no existe"}));        
            response.end();
            break;
    }
};


function manageTasks(method){
    let responseContent = "";
    let responseCode = "";
    let correct = false;

    if(method === constants.httpMethods.get){
        responseContent = model.tasks.getAll();
        responseCode = httpCodes.OK;
        correct = true;
    }else{
        responseCode = httpCodes.METHOD_NOT_ALLOWED;
        responseContent = "Error";
    }

    let response = {
        responseCode: responseCode,
        responseContent: responseContent,
        ok: correct
    };
    return response;
}

function manageTask(method, strParameter, body){
    let responseContent = "";
    let responseCode = httpCodes.OK;
    let msgError = "";
    let correct = true;
    let fecha;
    let nId;
    let currentTask;

    switch(method){ 
        case constants.httpMethods.get: 
            if(strParameter != ""){
                nId = Number(strParameter);
                if(Number.isInteger(nId) && !Number.isNaN(nId)){
                    let task = model.tasks.getById(nId)
                    if(task){
                        responseCode = httpCodes.OK;
                        responseContent = {
                            error: false,
                            mensajeError: "",
                            tarea: task
                        };
                    }else{
                        responseCode = httpCodes.BAD_REQUEST;
                        responseContent = {
                            error: true,
                            mensajeError: "Error: no existe una tarea con ese identificador"
                        };
                    }
                }else{
                    responseCode = httpCodes.BAD_REQUEST;
                    responseContent = {
                        error: true,
                        mensajeError: "Error: el id debe ser un número"
                    };
                }
            }else{
                responseCode = httpCodes.BAD_REQUEST;   
                responseContent = {
                    error: true,
                    mensajeError: "Error: se debe especificar un id"
                };             
            }
            break;

        case constants.httpMethods.post: 
            if(!body.id || body.id == ""){
                responseCode = httpCodes.BAD_REQUEST;
                msgError = "Error: el campo 'id' no puede estar vacío";
                correct = false;
            }else{
                nId = Number(body.id);
                if(Number.isInteger(nId) && !Number.isNaN(nId)){
                    let x = model.tasks.getById(nId);
                    if(model.tasks.getById(nId)){
                        responseCode = httpCodes.BAD_REQUEST;
                        msgError = "Error: ya existe una tarea con el id " + nId;
                        correct = false;
                    }
                }else{
                    responseCode = httpCodes.BAD_REQUEST;
                    msgError = "Error: el campo 'id' debe ser un número";
                    correct = false;
                }
            } 

            if(correct && body.texto == ""){
                responseCode = httpCodes.BAD_REQUEST;
                msgError = "Error: el campo 'texto' no puede estar vacío";
                correct = false;
            } 
            
            if(correct && body.fechaTope == "" ){
                responseCode = httpCodes.BAD_REQUEST;
                msgError = "Error: el campo 'fechaTope' no puede estar vacío";
                correct = false;
            }else{
                fecha = new moment(body.fechaTope, constants.dateFormat);
                if(correct && !fecha.isValid()){
                    responseCode = httpCodes.BAD_REQUEST;
                    msgError = "Error: el campo 'fechaTope' no tiene un formato válido. Formato esperado: '" + constants.dateFormat + "'";
                    correct = false;
                }
            }
            
            if(correct && body.prioridad == ""){
                responseCode = httpCodes.BAD_REQUEST;
                msgError = "Error: el campo 'prioridad' es obligatorio";
                correct = false;
            }else{
                if(correct && body.prioridad != constants.priorities.high && 
                        body.prioridad != constants.priorities.medium && 
                        body.prioridad != constants.priorities.low ){
                    responseCode = httpCodes.BAD_REQUEST;
                    msgError = "Error: el campo 'prioridad' no tiene un valor correcto";
                    correct = false;
                }
            }
            if(correct){
                if(!model.tasks.create(body.id, body.texto, body.fechaTope, body.prioridad)){
                    responseCode = httpCodes.INTERNAL_SERVER_ERROR;
                    msgError = "Error: se ha producido un error inesperado al guardar la tarea";
                    correct = false;
                }
            }

            responseContent = {
                error: !correct,
                mensajeError: msgError
            };     
            
            break;
            
        case constants.httpMethods.put: 
            if(!body.id || body.id == ""){
                responseCode = httpCodes.BAD_REQUEST;
                msgError = "Error: el campo 'id' no puede estar vacío";
                correct = false;
            }else{
                nId = Number(body.id);
                if(Number.isInteger(nId) && !Number.isNaN(nId)){
                    currentTask = model.tasks.getById(nId);
                    if(!currentTask && method == constants.httpMethods.put){
                        responseCode = httpCodes.BAD_REQUEST;
                        msgError = "Error: no existe una tarea con el id " + nId;
                        correct = false;
                    }
                }else{
                    responseCode = httpCodes.BAD_REQUEST;
                    msgError = "Error: el campo 'id' debe ser un número";
                    correct = false;
                }
            } 

            if(correct && body.fechaTope && body.fechaTope != "" ){
                fecha = new moment(body.fechaTope, constants.dateFormat);
                if(!fecha.isValid()){
                    responseCode = httpCodes.BAD_REQUEST;
                    msgError = "Error: el campo 'fechaTope' no tiene un formato válido. Formato esperado: '" + constants.dateFormat + "'";
                    correct = false;
                }
            }
            
            if(correct && body.prioridad && body.prioridad != ""){
                let tpmPrioridad = body.prioridad.toLowerCase();
                if(tpmPrioridad != constants.priorities.high && 
                        tpmPrioridad != constants.priorities.medium && 
                        tpmPrioridad != constants.priorities.low ){
                    responseCode = httpCodes.BAD_REQUEST;
                    msgError = "Error: el campo 'prioridad' no tiene un valor correcto";
                    correct = false;
                }
            }
            if(correct){
                if(!model.tasks.update(body.id, body.texto, body.fechaTope, body.prioridad)){
                    responseCode = httpCodes.INTERNAL_SERVER_ERROR;
                    msgError = "Error: se ha producido un error inesperado al guardar la tarea";
                    correct = false;
                }
            }

            if(correct){
                responseContent = {
                    error: !correct,
                    mensajeError: msgError,
                    tarea: currentTask
                };     
            }else{
                responseContent = {
                    error: !correct,
                    mensajeError: msgError
                };
            }
            
            break;
        case constants.httpMethods.delete: 
            if(strParameter != ""){
                nId = Number(strParameter);
                if(Number.isInteger(nId) && !Number.isNaN(nId)){
                    if(model.tasks.getById(nId)){
                        correct = model.tasks.delete(nId);
                        if(correct){
                            responseCode = httpCodes.OK;
                            responseContent = {
                                error: false,
                                mensajeError: ""
                            };
                        }else{
                            responseCode = httpCodes.INTERNAL_SERVER_ERROR;
                            responseContent = {
                                error: true,
                                mensajeError: "Error: Se ha producido un error inesperado al eliminar el archivo."
                            };
                        }
                    }else{
                        responseCode = httpCodes.BAD_REQUEST;
                        responseContent = {
                            error: true,
                            mensajeError: "Error: no existe una tarea con ese identificador"
                        };
                    }
                }else{
                    responseCode = httpCodes.BAD_REQUEST;
                    responseContent = {
                        error: true,
                        mensajeError: "Error: el id debe ser un número"
                    };
                }
            }else{
                responseCode = httpCodes.BAD_REQUEST;   
                responseContent = {
                    error: true,
                    mensajeError: "Error: se debe especificar un id"
                };             
            }
            break;
        default: 
            responseCode = httpCodes.METHOD_NOT_ALLOWED;
            responseContent = {
                error: false,
                mensajeError: "Error: verbo http no soportado"
            };     
            break;
    }

    let response = {
        responseCode: responseCode,
        responseContent: responseContent
    };

    return response;
}

function manageCurrents(method, parameter){
    let responseContent = "";
    let responseCode = httpCodes.OK;
    let correct = false;

    if(method != constants.httpMethods.get){
        responseCode = httpCodes.METHOD_NOT_ALLOWED;
    }else{
        if(!parameter || parameter == ""){
            responseContent = model.tasks.getActives();
        }else{
            if(parameter == constants.priorities.high ||
                    parameter == constants.priorities.medium || 
                    parameter == constants.priorities.low){
                responseContent = model.tasks.getActivesByPriority(parameter);
            }else{
                responseContent = [];
            }
        }
    }
    
    let response = {
        responseCode: responseCode,
        responseContent: responseContent,
        ok: correct
    };

    return response;
}


http.createServer(handleRequest).listen(constants.port);