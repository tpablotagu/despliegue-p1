let endpoints = {
    tasks: "/tareas",
    task: "/tarea",
    currents: "/tareas_vigentes",
};

let priorities = {
    high: "alta",
    medium: "media",
    low: "baja"
};

let responseTypes = {
    text: "text/plain",
    html: "text/html",
    jpg: "image/jpg",
    png: "image/png"
};

let methods = {
    get: "GET",
    post: "POST",
    put: "PUT",
    delete: "DELETE"
};

let dateFormat = "dd/MM/yyyy hh:mm";

let tasksFile = "./tareas.json";

let encoding = "utf8";

let port = 8080;

module.exports = {
    encoding: encoding,
    tasksFile: tasksFile,
    endpoints: endpoints,
    priorities: priorities,
    responseTypes: responseTypes,
    httpMethods: methods,
    port: port,
    dateFormat: dateFormat
}