const fs = require("fs");
const moment = require("moment");
const constants = require("./constants.js");


let save = (taskList) => {
    let saved = false;
    try {
        fs.writeFileSync(constants.tasksFile, JSON.stringify(taskList));
        saved = true;
    } catch (error) {
        console.log(error);
    }
    return saved;
}

let create = (id, description, date, priority) => {
    let created = false;
    try {
        if(!getById(id)){
            let tasks = getAll();
            let newTask = {
                id: id,
                texto: description,
                fechaTope: date,
                prioridad: priority
            }
            tasks.push(newTask);
            created = save(tasks);
        }     
    } catch (error) {
        console.log(error);
    }
    return created;
}

let remove = (id) => {
    let removed = false;
    try {
        let tasks = getAll();
        let filteredTasks = tasks.filter((task) => task.id != id);
        if(tasks.length != filteredTasks.length){
            removed = save(filteredTasks);
        }
    } catch (error) {
        console.log(error);
    }
    return removed;
}

let update = (id, description, date, priority) => {
    let updated = false;
    try {
        let tasks = getAll();
        let current  = tasks.filter((task) => task.id == id)[0];
        if(current != false){
            current.texto = description;
            current.fechaTope = date;
            current.prioridad = priority;
        }
        updated = save(tasks);
    } catch (error) {
        console.log(error);
    }
    return updated;
}

let getAll = () => {
    try {
        var x = fs.readFileSync(constants.tasksFile, constants.encoding);
        return JSON.parse(x);
    } catch (error) {
        console.log(error);
        return [];
    }
}

let getById = (id) => {
    try {
        let tasks = getAll();
        return tasks.filter((task) => task.id == id)[0];
    } catch (error) {
        console.log(error);
        return [];
    }
}

let getActives = () => {
    try {
        let tasks = getAll();
        let now = moment();
        let currentDate;
        let difference;
        return tasks.filter((task) => {
            currentDate = moment(task.fechaTope, constants.dateFormat);
            difference = now.diff(currentDate);
            if(difference > 0){
                return false;
            }else{
                return true;
            }
        });
    } catch (error) {
        console.log(error);
    }
}

let getActivesByPriority = (priority) => {
    try {
        let tasks = getActives();
        return tasks.filter((task) => task.prioridad.toLowerCase() == priority);
    } catch (error) {
        console.log(error);
    }
}


module.exports = {
    save: save, 
    create: create,
    update: update,
    delete: remove,
    getAll: getAll,
    getById: getById,
    getActives: getActives,
    getActivesByPriority: getActivesByPriority
}